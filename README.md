# 3088 PiHat project

We will design a microHAT that controls the motion of motors (left/right and up/down) for cameras with high precision using position feedback. 

Scenarios where using the microHAT would be applicable:
CCTV cameras monitoring people/activity in a store to prevent theft. (Security)
Cameras for a home security system monitoring areas to ensure protection. (Safety).
Cameras that capture the license plates of cars parked in a parking lot. (Recording information)
